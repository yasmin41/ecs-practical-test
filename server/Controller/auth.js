exports.auth = async (req, res) => {
  return res.status(200).json({
    status: true,
    role: req.user.role,
  });
};
