const mongoose = require("mongoose");

//create module
const { seedUser } = require("./admin-seeder");

mongoose
  .connect(`mongodb://localhost:27017/ecs-demo-api`)
  .then(async () => {
    console.log("mongodb connected, seeding data");
    await seed();
    await exit();
  })
  .catch((err) => {
    console.log(err);
  });

async function seed() {
  console.log("seed function called");
  await seedUser();
}

function exit() {
  console.log("seeding completed");
  mongoose.disconnect();
}
