const users = require("../models/users");
const bcrypt = require("bcryptjs");
const { userRole } = require("../constant/constant");

async function seed() {
  try {
    const encryptedPassword = await bcrypt.hash("123", 10);
    await new users({
      email: "yasminjejani123@gmail.com",
      password: encryptedPassword,
      name: "yasmin",
      role: userRole.ADMIN,
      phone_no: '1231231231'
    }).save();
  } catch (error) {
    console.log("erroe", error);
  }
}

module.exports.seedUser = seed;
