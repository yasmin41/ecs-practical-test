const express = require("express");
const router = express.Router();

//Controllers
const usersRouter = require("./users");
const authRouter = require("./auth");

//Module vise api
router.use("/users", usersRouter);
router.use("/auth", authRouter);


module.exports = router;
