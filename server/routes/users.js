const express = require("express");
const router = express.Router();

//create module
const {
  loginValidation,
  signupValidation,
  updateUserValidation,
} = require("../Validation/usersValidation");
const {
  loginUser,
  signupUser,
  userListing,
  userUpdate,
  userDelete,
  SingleUserListing,
} = require("../Controller/userController");
const AuthMiddleware = require("../Middleware/AuthMiddleware");
const { userRole } = require("../constant/constant");

//user login route
router.post("/login", loginValidation, loginUser);

//user signup route
router.post("/signup", signupValidation, signupUser);

//user listing
router.get(
  "/user-listing",
  AuthMiddleware([userRole.ADMIN, userRole.ENDUSER]),
  userListing
);

// //get single user
router.get("/:id", AuthMiddleware([userRole.ADMIN]), SingleUserListing);

//edit user
router.put("/:id", [AuthMiddleware([userRole.ADMIN]), updateUserValidation], userUpdate);

//delete user
router.delete("/:id", AuthMiddleware([userRole.ADMIN]), userDelete);

module.exports = router;
