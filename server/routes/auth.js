const express = require("express");
const { userRole } = require("../constant/constant");
const router = express.Router();

//create module
const AuthMiddleware = require("../Middleware/AuthMiddleware");
const { auth } = require("../Controller/auth");

//user listing
router.get("/", AuthMiddleware([userRole.ADMIN, userRole.ENDUSER]), auth);

module.exports = router;
