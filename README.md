### This project contains frontend (Client folder) and backend (server folder)

## Server 
#### Setup in local
```
cd server/
npm install
cp example.env .env (You can configure as env for your local in .env)
npm start
```

Once `npm start` command run successfully, API can accessible on `http://localhost:3001`

To import admin user directly to the db please run this command `node seed/index.js`. This will add one admin user directly into user's collection.

When you want to login as admin from frontend, please use this credentials `yasminjejani123@gmail.com / 123` 
## Client
#### Setup in local
```
cd client/
npm install
npm start
```
Once `npm start` command run successfully, client can accessible on `http://localhost:3000`
