import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const RequireAuth = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const [isAuthorised, setIsAuthorised] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    let token = localStorage.getItem("token");
    if (token !== null) {
      setIsAuthorised(true);
    }
    setLoading(false);
  }, []);

  if (loading && !isAuthorised) {
    return <h4>Validating.....</h4>;
  }

  if (!loading && isAuthorised) {
    return children;
  }

  if (!loading && !isAuthorised) {
    return navigate("/login");
  }
};
export default RequireAuth;
