import React, { createContext, useContext, useEffect, useState } from "react";
import { getUserAuth_api } from "../api_calls/api_calls";

const AuthContext = createContext({
  isLoading: null,
  isAuth: false,
  userRole: null,
});

export const useAuth = () => useContext(AuthContext);

function AuthProvider({ children }) {
  const [isLoading, setIsLoading] = useState(true);
  const [isAuth, setIsAuth] = useState(false);
  const [userRole, setUserRole] = useState(null);

  useEffect(() => {
    const userToken = localStorage.getItem("token")
      ? localStorage.getItem("token")
      : null;
    if (userToken) {
      getUserAuth_api()
        .then((response) => {
          setIsLoading(false);
          setUserRole(response?.data?.role);
          setIsAuth(true);
        })
        .catch(() => {
          setIsLoading(false);
          setUserRole(null);
          setIsAuth(false);
        });
    } else {
      setIsLoading(false);
      setUserRole(null);
      setIsAuth(false);
    }
  }, []);

  const value = {
    isLoading,
    userRole,
    isAuth,
    setIsAuth,
    setUserRole,
  };
  return (
    <AuthContext.Provider value={value}>
      {isLoading ? <> Loading... </> : children}
    </AuthContext.Provider>
  );
}

export default AuthProvider;
