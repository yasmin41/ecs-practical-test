import "./App.css";
import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from "./pages/Login";
import Signup from "./pages/Signup";
import Dashboard from "./pages/Dashboard";
import RequireAuth from "./authentication/RequireAuth";
import UpdateForm from "./pages/UpdateForm";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <div>
    <ToastContainer />
      <Router>
        <Routes>
          <Route path="/" exact element={<Signup/>}/>
            <Route path="/login" exact  element={<Login/> } />
            <Route path="/dashboard" exact element={<RequireAuth><Dashboard/></RequireAuth>} />
            <Route path="/update/:id" exact  element={<RequireAuth><UpdateForm/></RequireAuth> } />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
