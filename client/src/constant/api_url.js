import { base_url } from "./config";

export const signup_url = `${base_url}/api/users/signup`;
export const login_url = `${base_url}/api/users/login`;
export const getAllUsers_url = `${base_url}/api/users/user-listing`;
export const getUserAuth_url = `${base_url}/api/auth`;
export const deleteUser_url = (id) => `${base_url}/api/users/${id}`;
export const editUser_url = (id) => `${base_url}/api/users/${id}`;
export const getSingleUser_url = (id) => `${base_url}/api/users/${id}`;
