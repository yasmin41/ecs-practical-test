const env = "local";
const masterConfig = {
  local: {
    base_url: "http://localhost:3001",
  },
  staging: {
    base_url: "http://localhost:3001",
  },
  prod: {
    base_url: "http://localhost:3001",
  },
};

export const { base_url } = masterConfig[env];
