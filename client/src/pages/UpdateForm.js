import React, { useEffect, useState } from "react";
import Inputbox from "../componet/Inputbox";
import "bootstrap/dist/css/bootstrap.css";
import { Row, Container, Col } from "react-bootstrap";
import { Form } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate, useParams } from "react-router-dom";
import { editUser_api, getSingleUser_api } from "../api_calls/api_calls";
import { useAuth } from "../context/AuthProvider";
import { updateUserValidation } from "../validation/usersValidation";

const UpdateForm = () => {
  const [updateDetail, setUpdateDetail] = useState({
    name: "",
    email: "",
    phone_no: "",
  });
  const navigate = useNavigate();
  let { id } = useParams();
  const { userRole } = useAuth();

  useEffect(() => {
    if (id) {
      getSingleUser_api(id)
        .then((response) => {
          console.log(response?.data?.result?.user);
          setUpdateDetail({
            name: response?.data?.result?.user?.name,
            email: response?.data?.result?.user?.email,
            phone_no: response?.data?.result?.user?.phone_no,
          });
        })
        .catch((error) => {
          toast.error(error?.errors?.[0] || error?.response?.data?.message);
        });
    }
  }, [id]);

  useEffect(() => {
    if (userRole !== "admin") navigate("/dashboard");
  }, []);
  const handleOnChange = (event) => {
    setUpdateDetail({
      ...updateDetail,
      ...{ [event.target.name]: event.target.value },
    });
  };

  const handleOnSubmit = (event) => {
    event.preventDefault();
    if (!updateUserValidation(updateDetail)) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      editUser_api(id, {
        name: updateDetail.name,
        email: updateDetail.email,
        phone_no: updateDetail.phone_no,
      })
        .then((response) => {
          //  notify user that he signed up successfully
          toast.success("Data Updated Succesfully", response);
          navigate("/dashboard");
        })
        .catch((error) => {
          toast.error(
            error?.errors?.[0] ||
              error?.response?.data?.message ||
              error?.response?.data?.errors
          );
        });
    }
  };

  return (
    <Container>
      <Row className="justify-content-center">
        <div className="form-holder">
          <Col md={8}>
            <h3>Update Form</h3>
            <Form onSubmit={handleOnSubmit}>
              <Row>
                <Inputbox
                  label="Name"
                  type="text"
                  name="name"
                  value={updateDetail.name}
                  onChange={handleOnChange}
                  classname="col-sm-12"
                />
              </Row>
              <Row>
                <Inputbox
                  label="Email"
                  type="email"
                  name="email"
                  readOnly={true}
                  value={updateDetail.email}
                  onChange={handleOnChange}
                  validFeedback="valid"
                  invalidFeedBack="Plese Enter Valid Email Address"
                  classname="col-sm-12"
                />
              </Row>
              <Row>
                <Inputbox
                  label="Phone No"
                  type="text"
                  name="phone_no"
                  value={updateDetail.phone_no}
                  onChange={handleOnChange}
                  classname="col-sm-12"
                />
              </Row>
              <Button variant="success" type="submit">
                Submit
              </Button>
              &nbsp;
              <Button variant="danger" onClick={() => navigate("/dashboard")}>
                Cancel
              </Button>
            </Form>
          </Col>
        </div>
      </Row>
    </Container>
  );
};
export default UpdateForm;
