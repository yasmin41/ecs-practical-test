import React, { useState } from "react";
import Inputbox from "../componet/Inputbox";
import "bootstrap/dist/css/bootstrap.css";
import { Row, Container, Col } from "react-bootstrap";
import { Form } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Link, useNavigate } from "react-router-dom";
import { login_api } from "../api_calls/api_calls";
import { useAuth } from "../context/AuthProvider";
import { loginValidation } from "../validation/usersValidation";

const Login = () => {
  const { setIsAuth, setUserRole } = useAuth();
  const [loginDetail, setloginDetail] = useState({
    email: "",
    password: "",
  });
  const navigate = useNavigate();

  const handleOnChange = (event) => {
    setloginDetail({
      ...loginDetail,
      ...{ [event.target.name]: event.target.value },
    });
  };

  const handleOnSubmit = async (event) => {
    event.preventDefault();
    if (!(await loginValidation(loginDetail))) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      login_api({ email: loginDetail.email, password: loginDetail.password })
        .then((response) => {
          //  notify user that he signed up successfully
          toast.success("you are register successfully", response);
          // store token in localstorage
          localStorage.setItem("token", response?.data?.result?.token);
          // Update context variables
          setIsAuth(true);
          setUserRole(response?.data?.result?.role);
          // Redirect user to dashboard page
          navigate("/dashboard");
        })
        .catch((error) => {
          toast.error(error?.errors?.[0] || error?.response?.data?.message);
        });
    }
  };

  return (
    <>
      <Container>
        <Row className="justify-content-center">
          <div className="form-holder">
            <Col md={8}>
              <h3>Log In</h3>
              <Form onSubmit={handleOnSubmit}>
                <Row>
                  <Inputbox
                    label="Email"
                    type="email"
                    name="email"
                    value={loginDetail.email}
                    onChange={handleOnChange}
                    classname="col-sm-12"
                  />
                </Row>
                <Row>
                  <Inputbox
                    label="password"
                    type="password"
                    name="password"
                    value={loginDetail.password}
                    onChange={handleOnChange}
                    classname="col-sm-12"
                  />
                </Row>
                <Button type="submit">Submit</Button>
              </Form>
              <Col md={12} className="pt-2">
                Not sign up yet? <Link to={"/"}>Click here</Link>
              </Col>
            </Col>
          </div>
        </Row>
      </Container>
    </>
  );
};
export default Login;
