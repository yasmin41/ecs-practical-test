import React, { useState } from "react";
import Inputbox from "../componet/Inputbox";
import "bootstrap/dist/css/bootstrap.css";
import { Row, Container, Col } from "react-bootstrap";
import { Form } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate, Link } from "react-router-dom";
import { signup_api } from "../api_calls/api_calls";
import { signupValidation } from "../validation/usersValidation";

const Signup = () => {
  const [loginDetail, setloginDetail] = useState({
    name: "",
    email: "",
    password: "",
    phone_no: "",
  });
  const navigate = useNavigate();

  const handleOnChange = (event) => {
    setloginDetail({
      ...loginDetail,
      ...{ [event.target.name]: event.target.value },
    });
  };

  const handleOnSubmit = async (event) => {
    event.preventDefault();
    if (!(await signupValidation(loginDetail))) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      signup_api({
        name: loginDetail.name,
        email: loginDetail.email,
        password: loginDetail.password,
        phone_no: loginDetail.phone_no,
      })
        .then((response) => {
          //  notify user that he signed up successfully
          toast.success("you are register successfully", response);
          navigate("/login");
        })
        .catch((error) => {
          toast.error(
            error?.errors?.[0] ||
              error?.response?.data?.message ||
              error?.response?.data?.errors
          );
        });
    }
  };

  return (
    <Container>
      <Row className="justify-content-center">
        <div className="form-holder">
          <Col md={8}>
            <h3>Sign Up</h3>
            <Form onSubmit={handleOnSubmit}>
              <Row>
                <Inputbox
                  label="Name"
                  type="text"
                  name="name"
                  value={loginDetail.name}
                  onChange={handleOnChange}
                  classname="col-sm-12"
                />
              </Row>
              <Row>
                <Inputbox
                  label="Email"
                  type="email"
                  name="email"
                  value={loginDetail.email}
                  onChange={handleOnChange}
                  classname="col-sm-12"
                />
              </Row>
              <Row>
                <Inputbox
                  label="password"
                  type="password"
                  name="password"
                  value={loginDetail.password}
                  onChange={handleOnChange}
                  classname="col-sm-12"
                />
              </Row>
              <Row>
                <Inputbox
                  label="Phone No"
                  type="text"
                  name="phone_no"
                  value={loginDetail.phone_no}
                  onChange={handleOnChange}
                  classname="col-sm-12"
                />
              </Row>
              <Button type="submit">Submit</Button>
            </Form>
            <Col md={12} className="pt-2">
              Already user? <Link to={"/login"}>Click here</Link>
            </Col>
          </Col>
        </div>
      </Row>
    </Container>
  );
};
export default Signup;
