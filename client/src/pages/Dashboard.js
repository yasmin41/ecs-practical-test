import React from "react";
import Showdatatable from "../componet/Showdatatable";
import { useNavigate } from "react-router-dom";
import { Button, Col, Container, Row } from "react-bootstrap";

const Dashboard = () => {
  const navigate = useNavigate();

  const handleLogout = () => {
    localStorage.removeItem("token");
    navigate("/login");
  };

  return (
    <>
      <Container>
        <Row>
          <Col md={12} className="pb-3 pt-3" style={{textAlign: 'right'}}>
            <Button onClick={handleLogout} variant="primary">
              Logout
            </Button>
          </Col>
        </Row>
        <Row>
          <Showdatatable />
        </Row>
      </Container>
    </>
  );
};
export default Dashboard;
