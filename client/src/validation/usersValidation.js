import { toast } from "react-toastify";

import * as yup from "yup";

export const signupValidation = (formData) => {
  const phoneRegExp =
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
  const signupSchema = yup.object().shape({
    name: yup.string().required('Name is required'),
    email: yup.string().email('Please enter valid email').required('Email is required'),
    password: yup.string().required('Password is required'),
    phone_no: yup
      .string()
      .matches(phoneRegExp, "Phone number is not valid")
      .min(10, "Phone number is not valid")
      .max(10, "Phone number is not valid")
      .required(),
  });
  return signupSchema
    .validate(formData)
    .then(() => true)
    .catch((error) => {
      toast.error(error.errors[0] || "Something went wrong");
      return false;
    });
};
export const loginValidation = (formData) => {
  const loginSchema = yup.object().shape({
    email: yup.string().email().required(),
    password: yup.string().required(),
  });
  return loginSchema
    .validate(formData)
    .then(() => true)
    .catch((error) => {
      toast.error(error.errors[0] || "Something went wrong");
      console.log("some error in login validation");
      return false;
    });
};
export const updateUserValidation = (formData) => {
  const phoneRegExp =
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
  const updateUserSchema = yup.object().shape({
    name: yup.string().required(),
    phone_no: yup
      .string()
      .matches(phoneRegExp, "Phone number is not valid")
      .min(10, "Phone number is not valid")
      .max(10, "Phone number is not valid")
      .required(),
  });
  return updateUserSchema
    .validate(formData)
    .then(() => true)
    .catch((error) => {
      toast.error(error.errors[0] || "Something went wrong");
      return false;
    });
};
