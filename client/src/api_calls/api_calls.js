import axios from "axios";
import {
  login_url,
  signup_url,
  getAllUsers_url,
  getUserAuth_url,
  deleteUser_url,
  editUser_url,
  getSingleUser_url,
} from "../constant/api_url";
import authHeader from "../utils/AuthHeader";

export const login_api = (data) => axios.post(login_url, data);
export const signup_api = (data) => axios.post(signup_url, data);
export const getAllUsers_api = () =>
  axios.get(getAllUsers_url, { headers: authHeader() });
export const getUserAuth_api = () =>
  axios.get(getUserAuth_url, { headers: authHeader() });
export const deleteUser_api = (id) => axios.delete(deleteUser_url(id), { headers: authHeader() });
export const editUser_api = (id, data) =>
  axios.put(editUser_url(id), data, { headers: authHeader() });
export const getSingleUser_api = (id) =>
  axios.get(getSingleUser_url(id), { headers: authHeader() });
