import React, { useEffect, useState } from "react";
import Table from "react-bootstrap/Table";
import { useNavigate } from "react-router-dom";
import { deleteUser_api, getAllUsers_api } from "../api_calls/api_calls";
import { ToastContainer, toast } from "react-toastify";
import { Button, Row, Col } from "react-bootstrap";
import { useAuth } from "../context/AuthProvider";

const Showdatatable = (props) => {
  const [users, setUsers] = useState();
  const { userRole } = useAuth();

  const navigate = useNavigate();

  useEffect(() => {
    getAllUsers_api()
      .then((response) => {
        console.log(response?.data?.result?.users);
        setUsers(response?.data?.result?.users);
      })
      .catch((error) => {
        toast.error(error?.errors?.[0] || error?.response?.data?.message);
      });
  }, []);
  const onEditHandler = (id) => {
    navigate(`/update/${id}`);
  };
  const onDeleteHandler = (id, index) => {
    deleteUser_api(id)
      .then((response) => {
        console.log(response);
        toast.success("Deleted User Data Succesfully", response);
        const newUser = JSON.parse(JSON.stringify(users));
        newUser.splice(index, 1);
        setUsers(newUser);
      })
      .catch((error) => {
        toast.error(error?.errors?.[0] || error?.response?.data?.message);
      });
  };

  return (
    <Row>
      <Col md={12} className="pb-3 pt-3">
        <Table variant="dark"  striped bordered hover responsive>
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Phone No</th>
              {userRole === "admin" ? <th>Action</th> : null}
            </tr>
          </thead>

          {users &&
            users.map((user, index) => {
              return (
                <tbody>
                  <tr key={index}>
                    <td>{user.name}</td>
                    <td>{user.email}</td>
                    <td>{user.phone_no}</td>
                    {userRole === "admin" ? (
                      <td>
                        <Button
                          variant="primary"
                          onClick={() => onEditHandler(user._id)}
                        >
                          Edit{" "}
                        </Button>{" "}
                        &nbsp;
                        <Button
                          variant="danger"
                          onClick={() => onDeleteHandler(user._id, index)}
                        >
                          Delete
                        </Button>
                      </td>
                    ) : null}
                  </tr>
                </tbody>
              );
            })}
        </Table>
      </Col>
    </Row>
  );
};

export default Showdatatable;
